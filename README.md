# Hello World

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

Use node v20 use :
```bash
nvm install 20
```

```bash
npm i
```

## Usage

Start the application dev with :

```bash
npm run start
```

Created the dist with :

```bash
npm run dist
```

Analyse the coding rules with :

```bash
npm run lint
```

## Configuration de l'API ChatGPT

Pour que le bot puisse répondre à la commande `ask`, vous devez renseigner votre clé API ChatGPT dans le fichier `api.js`. Voici les étapes à suivre :

1. **Obtenir une clé API ChatGPT**
   - Rendez-vous sur [OpenAI API](https://platform.openai.com/signup/) pour créer un compte et obtenir votre clé API.

2. **Ajouter la clé API dans `api.js`**
   - Ouvrez `api.js` et remplacez `YOUR_API_KEY` par votre clé API