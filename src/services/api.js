// src/services/api.js
import { OpenAI } from 'openai';

const OPENAI_API_KEY = 'YOUR_API_KEY'; // Remplacez par votre clé API

const openai = new OpenAI({
  apiKey: OPENAI_API_KEY,
  dangerouslyAllowBrowser: true // Désactiver la sécurité
});

const askGPT = async (prompt) => {
  try {
    const response = await openai.completions.create({
      model: 'text-davinci-003', // Utilisez le modèle approprié
      prompt,
      max_tokens: 150,
      temperature: 0.7
    });

    return response.choices[0].text.trim();
  } catch (error) {
    /* eslint-disable no-console */
    console.error('Error communicating with OpenAI:', error);
    /* eslint-enable no-console */
    return 'Sorry, there was an error processing your request.';
  }
};

export default askGPT;
